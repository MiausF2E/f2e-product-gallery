const { name, dependencies } = require('./package.json');

module.exports = {
  baseUrl: `/${name}/`,
  transpileDependencies: Object.keys(dependencies),
};
